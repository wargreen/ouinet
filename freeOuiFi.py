#!/usr/bin/python3

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

######### USAGE ##########
# 1. Enter your data here then launch the script.
# 2. Wait for the welcome message
# 3. ????
# 4. Enjoy !

myDatas = {
	"bookingNumber":"", # Reservation
	"carriage":"", # Voiture
	"firstName":"", # Nom
	"lastName":"", # Prénom
	"seat":"", # Siège
	"conditions":"true", # Conditions d'utilisation acceptées
	}


# Script start

import requests
import json
import datetime
import time


user_agent = {'User-Agent': 'Mozilla/5.0 \
	(Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7)\
	 Gecko/2009021910 Firefox/3.0.7', }


login_resp = requests.post("https://ouifi.ouigo.com:8084/api/login/fallback", headers=user_agent, json=myDatas)

if login_resp.status_code != 201:
	print("Login error :", login_resp.text)
	quit()

token = login_resp.json()
token = token["session"]["token"]
print("Login OK")
print("Token :", token)

auth = {"authorization": "Bearer "+token}
headers = {**user_agent, **auth}

while True:
	open_resp = requests.get("https://ouifi.ouigo.com:8084/api/payment/worldline/information", headers=headers)

	if open_resp.status_code == 200:
		print("Welcome to the Internet")
		time.sleep(120)
	else:
		print("Error, http response :", open_resp)
		print("Retry in 10s")
		time.sleep(10)

